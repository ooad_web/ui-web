import type Employee from "@/types/Employee";
import http from "./axios";

function getEmployees() {
  return http.get("/employees");
}
function getEmployeeByID(id: number) {
  return http.get(`/employees/${id}`);
}

function saveEmployees(employee: Employee) {
  return http.post("/employees", employee);
}
function updateEmployees(id: number, user: Employee) {
  return http.patch(`/employees/${id}`, user);
}
function deleteEmployees(id: number) {
  return http.delete(`/employees/${id}`);
}

export default {
  getEmployees,
  saveEmployees,
  updateEmployees,
  deleteEmployees,
  getEmployeeByID,
};
