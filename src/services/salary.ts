import type Salary from "@/types/Salary";
import http from "./axios";

function findAllForPay() {
  return http.get(`/checkinout/findAllForPay`);
}
function saveSalary(salary: Salary) {
  return http.post("/salarys", salary);
}
export default { findAllForPay, saveSalary };
