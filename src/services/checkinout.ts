import type Checkinout from "@/types/Checkinout";
import http from "./axios";
function getCheckinout() {
  return http.get("/checkinout");
}

function getCheckinoutByID(id: number) {
  return http.get(`/checkinout/${id}`);
}

function saveCheckinout(employee: Checkinout) {
  return http.post("/checkinout", employee);
}

function updateCheckinout(id: number, checInOut: Checkinout) {
  console.log("updated");
  return http.patch(`/checkinout/${id}`, checInOut);
}

function daleteCheckinout(id: number) {
  return http.delete(`/checkinout/${id}`);
}
export default {
  getCheckinout,
  updateCheckinout,
  daleteCheckinout,
  saveCheckinout,
  getCheckinoutByID,
};
