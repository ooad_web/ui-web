import http from "./axios";
import type ProductQueue from "@/types/ProductQueue";
function getProductQueues() {
  return http.get("/product-queue");
}

function getProductQueueWithStatus(status: string) {
  return http.get(`/product-queue/status/${status}`);
}

function saveProductQueue(productQueue: ProductQueue) {
  return http.post("/product-queue", productQueue);
}

function updateProductQueue(id: number, productQueue: ProductQueue) {
  return http.patch(`/product-queue/${id}`, productQueue);
}

function deleteProductQueue(id: number) {
  return http.delete(`/product-queue/${id}`);
}

function changeStatus(id: number[], status: string) {
  return http.patch(`/product-queue/status/`, {
    ids: id,
    status: status,
  });
}

export default {
  getProductQueues,
  getProductQueueWithStatus,
  changeStatus,
  saveProductQueue,
  updateProductQueue,
  deleteProductQueue,
};
