import type Order from "@/types/Order";
import http from "./axios";
function getOrders() {
  return http.get("/orders");
}

function saveOrder(order: Order) {
  return http.post("/orders", order);
}

function updateOrder(id: number, order: Order) {
  return http.patch(`/orders/${id}`, order);
}

function deleteOrder(id: number) {
  return http.delete(`/orders/${id}`);
}

function billByTable(id: number) {
  return http.get(`/orders/bill/${id}`);
}

function checkoutBill(id: number) {
  return http.post("/orders/checkout/", {
    order: id,
  });
}

export default {
  getOrders,
  saveOrder,
  updateOrder,
  billByTable,
  checkoutBill,
  deleteProduct: deleteOrder,
};
