import type User from "@/types/User";
import http from "./axios";
function getUsers() {
  return http.get("/users");
}

function userLogin(login: string, password: string) {
  return http.post("/users/login", { login, password });
}

function saveUser(user: User) {
  return http.post("/users", user);
}

function updateUser(id: number, user: User) {
  return http.patch(`/users/${id}`, user);
}

function deleteUser(id: number) {
  return http.delete(`/users/${id}`);
}

function getUserByID(id: number) {
  return http.get(`/users/${id}`);
}

function getEmpByUserID(id: number) {
  return http.get(`/users/${id}`);
}
// function getEmployees() {
//   return http.get("/employees");
// }
// function saveEmployees(employee: Employee) {
//   return http.post("/employees", employee);
// }
// function updateEmployees(id: number, user: Employee) {
//   return http.patch(`/employees/${id}`, user);
// }
// function deleteEmployees(id: number) {
//   return http.delete(`/employees/${id}`);
// }

export default {
  getUsers,
  saveUser,
  updateUser,
  deleteUser,
  userLogin,
  getUserByID,
  getEmpByUserID,
};
