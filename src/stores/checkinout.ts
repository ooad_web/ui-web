import { ref, watch } from "vue";
import { defineStore } from "pinia";
import checkinoutService from "@/services/checkinout";
import { useUsersStore } from "./users";
import type Users from "@/types/Users";
import { useMessageStore } from "./message";
import { useLoadingStore } from "./loading";
import userService from "@/services/user";
import type Checkinout from "@/types/Checkinout";
import auth from "@/services/auth";
import CheckInOutViewVue from "@/views/CheckInOutView.vue";
import employee from "@/services/employee";
import UsersViewVue from "@/views/UsersView.vue";

export const usecheckinoutStore = defineStore("", () => {
  const checkinouts = ref<Checkinout[]>([
    {
      hour: 0,
      status: "",
    },
  ]);

  const checkin = async (username: string, password: string): Promise<void> => {
    try {
      const res = await UsersViewVue.checkin(username, password);
      console.log(res);
    } catch (e) {
      messageStore.showError("Username หรือ Password ไม่ถูกต้อง");
    }
    // localStorage.setItem("token", userName);
  };

  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const dialog = ref(false);

  const editedCheckinout = ref<{ username: string; password: string }>({
    username: "",
    password: "",
  });

  watch(dialog, (newDialog, oldDialog) => {
    console.log(newDialog);
    if (!newDialog) {
      editedCheckinout.value = {
        password: "",
        username: "",
      };
    }
  });

  async function getCheckinouts() {
    try {
      const res = await checkinoutService.getCheckinout();
      checkinouts.value = res.data;
      console.log(res.data);
    } catch (e) {
      console.log(e);
    }
  }
  const getCheckinout = async (id: number) => {
    return await checkinoutService.getCheckinoutByID(id);
  };

  async function saveCheckinout() {
    let isValid = true;
    try {
      const res = await userService.userLogin(
        editedCheckinout.value.username,
        editedCheckinout.value.password
      );
      console.log(res.data);

      if (res.data !== false) {
        console.log(res.data);
        for (let i = 0; i < checkinouts.value.length; i++) {
          const element = checkinouts.value[i];
          if (
            element.employee?.id === res.data.employee?.id &&
            element.hour === 0
          ) {
            console.log(
              "true " + element.employee?.id + " " + res.data.employee?.id
            );
            isValid = false;
            messageStore.showError("มีชื่ออยู่ในระบบแล้ว");
            console.log("true");
            break;
          }
        }
        if (isValid) {
          const checkInOut: Checkinout = {
            employee: res.data.employee,
            hour: 0,
            status: "ยังไม่ชำระ",
          };
          await checkinoutService.saveCheckinout(checkInOut);
        }
      } else {
        isValid = false;
        messageStore.showError("Username หรือ Password ผิด");
        console.log("false");
      }
    } catch (e) {
      console.log(e);
      isValid = false;
      console.log("unknow");
    }
    return isValid;
  }
  async function deleteEmployee(id: number) {
    loadingStore.isLoading = true;
    try {
      const res = await checkinoutService.daleteCheckinout(id);
      await getCheckinouts();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถลบข้อมูล Employee ได้");
    }
    loadingStore.isLoading = false;
  }

  async function deleteCheckinout(id: number) {
    loadingStore.isLoading = true;
    try {
      const res = await checkinoutService.daleteCheckinout(id);
      await getCheckinouts();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถลบ CheckIn-Out ได้");
    }
    loadingStore.isLoading = false;
  }

  function editCheckinout(checkinout: Checkinout) {
    editedCheckinout.value = JSON.parse(JSON.stringify(checkinout));
    dialog.value = true;
  }

  async function checkOut(checkinout: Checkinout) {
    console.log(checkinout.hour);
    try {
      const diff =
        new Date().valueOf() - new Date(checkinout.timestart!).getTime();
      const diffInHours = diff / 1000 / 60 / 60;
      const newcheckinout: Checkinout = {
        timeend: new Date(),
        hour: diffInHours,
      };
      if (Math.floor(diffInHours) !== 0) {
        console.log(diffInHours);
        await checkinoutService.updateCheckinout(checkinout.id!, newcheckinout);
      } else {
        console.log("เวลาน้อยกว่า 1 ชั่วโมง");
      }
    } catch (e) {
      console.log(e);
    }
  }

  return {
    dialog,
    checkOut,
    checkin,
    getCheckinouts,
    checkinouts,
    getCheckinout,
    editedCheckinout,
    deleteCheckinout,
    saveCheckinout,
    editCheckinout,
  };
});
