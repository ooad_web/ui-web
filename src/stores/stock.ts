import { ref, watch } from "vue";
import { defineStore } from "pinia";
import type Stock from "@/types/Stock";
import stockService from "@/services/stock";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";

export const useStockStore = defineStore("stock", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const dialog = ref(false);
  // let lastID = 4;

  watch(dialog, (newDialog, oldDialog) => {
    if (!newDialog) {
      editedStock.value = {
        id: 0,
        matCode: "",
        name: "",
        balance: 0,
        unit: "",
        category: "",
      };
    }
  });

  const stocks = ref<Stock[]>([
    // {
    //   id: 1,
    //   matCode: "RM-1",
    //   name: "ข้าวสาร",
    //   balance: 25,
    //   unit: "ถุง",
    //   category: "RawMaterial",
    // },
    // {
    //   id: 2,
    //   matCode: "FG-1",
    //   name: "น้ำเปล่า",
    //   balance: 100,
    //   unit: "ขวด",
    //   category: "FinishedGoods",
    // },
    // {
    //   id: 3,
    //   matCode: "FG-2",
    //   name: "โค้ก",
    //   balance: 63,
    //   unit: "ขวด",
    //   category: "FinishedGoods",
    // },
  ]);

  const getStocks = async () => {
    try {
      const res = await stockService.getStocks();
      stocks.value = res.data;
      console.log(res.data);
    } catch (error) {
      console.log(error);
    }
    return;
  };

  async function saveStock() {
    try {
      loadingStore.isLoading = true;
      const res = await stockService.saveStock(editedStock.value);
      dialog.value = false;
      await getStocks();
      loadingStore.isLoading = false;
    } catch (e) {
      console.log(e);
    }
  }

  // const delItemInStock = (matCode: string): void => {
  //   const index = stocks.value.findIndex((item) => item.matCode === matCode);
  //   stocks.value.splice(index, 1);
  // };

  async function deleteStock(id: number) {
    loadingStore.isLoading = true;
    try {
      const res = await stockService.deleteStock(id);
      await getStocks();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถลบข้อมูล Stock ได้");
    }
    loadingStore.isLoading = false;
  }

  const editedStock = ref<Stock>({
    id: 0,
    matCode: "",
    name: "",
    balance: 0,
    unit: "",
    category: "",
  });

  // if (editedStock.value.id < 0) {
  //   // Add new
  //   editedStock.value.id = lastID++;
  //   stocks.value.push(editedStock.value);
  // } else {
  //   const index = stocks.value.findIndex(
  //     (item) => item.id === editedStock.value.id
  //   );
  //   stocks.value[index] = editedStock.value;
  // }
  // dialog.value = false;
  // clear();

  const clear = () => {
    editedStock.value = {
      id: 0,
      matCode: "",
      name: "",
      balance: 0,
      unit: "",
      category: "",
    };
  };

  const editStock = (stock: Stock) => {
    // JSON.parse(JSON.stringify(user));
    // editedStock.value = { ...stock };
    editedStock.value = JSON.parse(JSON.stringify(stock));
    dialog.value = true;
  };

  return {
    stocks,
    // delItemInStock,
    dialog,
    saveStock,
    clear,
    editStock,
    editedStock,
    getStocks,
    deleteStock,
  };
});
