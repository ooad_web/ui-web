import { ref, computed } from "vue";
import { defineStore } from "pinia";
import productQueueService from "@/services/productQueue";
import type ProductQueue from "@/types/ProductQueue";

export const useProductQueueStore = defineStore("productQueue", () => {
  const productQueues = ref<ProductQueue[]>([]);
  const getProductQueue = async () => {
    try {
      const res = await productQueueService.getProductQueues();
      productQueues.value = res.data;
    } catch (error) {
      console.log(error);
    }
    return;
  };

  return { getProductQueue, productQueues };
});
