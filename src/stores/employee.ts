import { ref, watch } from "vue";
import { defineStore } from "pinia";
import type Employee from "@/types/Employee";
import employeeService from "@/services/employee";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";

export const useEmployeeStore = defineStore("Emp", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const dialog = ref(false);
  const employees = ref<Employee[]>([]);
  const editedEmployee = ref<Employee>({
    id: 0,
    firstname: "",
    lastname: "",
    tel: "",
    email: "",
    position: "",
    hour: -1,
    time_inout: "",
    date: "",
  });

  watch(dialog, (newDialog, oldDialog) => {
    if (!newDialog) {
      editedEmployee.value = {
        id: 0,
        firstname: "",
        lastname: "",
        tel: "",
        email: "",
        position: "",
        hour: -1,
        time_inout: "",
        date: "",
      };
    }
  });
  async function getEmployees() {
    loadingStore.isLoading = true;
    try {
      const res = await employeeService.getEmployees();
      employees.value = res.data;
      console.log(res);
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถเรียกข้อมูล Employee ได้");
    }
    loadingStore.isLoading = false;
  }
  const getEmployee = async (id: number) => {
    return await employeeService.getEmployeeByID(id);
  };
  async function saveEmployees() {
    loadingStore.isLoading = true;
    try {
      const res = await employeeService.saveEmployees(editedEmployee.value);
      dialog.value = false;
      await getEmployees();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถบันทึกข้อมูล Employee ได้");
    }
    loadingStore.isLoading = false;
  }
  async function deleteEmployee(id: number) {
    loadingStore.isLoading = true;
    try {
      const res = await employeeService.deleteEmployees(id);
      await getEmployees();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถลบข้อมูล Employee ได้");
    }
    loadingStore.isLoading = false;
  }

  function editEmployee(employee: Employee) {
    editedEmployee.value = JSON.parse(JSON.stringify(employee));
    dialog.value = true;
  }

  const clear = () => {
    editedEmployee.value = {
      id: 0,
      firstname: "",
      lastname: "",
      tel: "",
      email: "",
      position: "",
      hour: -1,
      time_inout: "",
      date: "",
    };
  };

  return {
    dialog,
    employees,
    getEmployees,
    saveEmployees,
    deleteEmployee,
    editEmployee,
    editedEmployee,
    getEmployee,
    clear,
  };
});
