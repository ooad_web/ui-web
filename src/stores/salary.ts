import { ref } from "vue";
import { defineStore } from "pinia";
import type Salary from "@/types/Salary";
import salaryService from "@/services/salary";
import type Checkinout from "@/types/Checkinout";
import checkinoutService from "@/services/checkinout";
import employeeService from "@/services/employee";
import type Employee from "@/types/Employee";

export const useSalaryStore = defineStore("Salary", () => {
  // let salaryFoyPay: any = [];
  const salaryFoyPay = ref<
    {
      total_hours: number;
      employee_firstname: string;
      employee_lastname: string;
      employee_position: string;
      employee_id: number;
    }[]
  >([]);
  async function getSalaryForPay() {
    try {
      const res = await salaryService.findAllForPay();
      salaryFoyPay.value = res.data;
      console.log(res.data);
      console.log(salaryFoyPay.value);
    } catch (e) {
      console.log(e);
    }
  }
  async function updateCheckInOut(
    checkInOut: Checkinout[],
    idEmployee: number,
    total: number
  ) {
    const newChekInOut: Checkinout = {
      status: "ชำระแล้ว",
    };
    const res = await employeeService.getEmployeeByID(idEmployee);
    const emp = ref<Employee>();
    emp.value = res.data;
    checkInOut.forEach(async (element) => {
      if (
        element.employee?.id === idEmployee &&
        element.status === "ยังไม่ชำระ"
      ) {
        await checkinoutService.updateCheckinout(element.id!, newChekInOut);
      }
    });
    const newSalary: Salary = {
      employee: emp.value,
      salary: total,
      date: new Date(),
    };
    await salaryService.saveSalary(newSalary);
    await getSalaryForPay();
  }
  return {
    getSalaryForPay,
    salaryFoyPay,
    updateCheckInOut,
  };
});
