import type Employee from "./Employee";

export default interface Salary {
  id?: number;
  date: Date;
  employee?: Employee;
  salary?: number;
}
