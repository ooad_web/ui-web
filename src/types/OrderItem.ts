import type Order from "./Order";
import type ProductQueue from "./ProductQueue";
import type Products from "./Products";
import type Table from "./Table";

export default interface OrderItem {
  id?: number;
  name?: string;
  price?: number;
  amount?: number;
  total?: number;
  order?: Order;
  note?: string;
  level?: string;
  product?: Products;
  table?: Table;
  productQueue?: ProductQueue[];

  // choice?: number;
  // choice_label?: string;
  // choice_value?: number;
  // addition?: string;
  // order_id?: number;
}

// type Level = {
//   name: string;
//   price: number;
// };
