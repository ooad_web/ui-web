export default interface OrderServe {
  id: number;
  table: number;
  name: String;
  note: string;
  level: String;
  num: number;
  status: string;
}
