import type Employee from "./Employee";

export default interface Checkinout {
  id?: number;
  timestart?: Date;
  timeend?: Date;
  hour?: number;
  status?: string;
  employee?: Employee;
}
